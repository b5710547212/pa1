/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class TaskTimer {

	/**Initial StopWatch*/
	private static final StopWatch timer = new StopWatch();
	
	public TaskTimer(){}
	
	/**
	 * elasped time for each runnable
	 * @param runnable
	 */
	public void timedAndPrint(Runnable runnable)
	{
		timer.start();
		runnable.run();
		timer.stop();
		
		System.out.println("Task : " + runnable.toString());
		System.out.println("Elapsed time is : " + timer.getElapsed());
	}
	
}
