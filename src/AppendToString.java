/**
 * Append to String speed test.
 * @author Woramate Jumroonsilp
 *
 */
public class AppendToString implements Runnable{

	/**A char for add to String*/
	private static final char CHAR = 'a';
	/**Name of this task*/
	private String task = "Append to String";
	/**Counter for value of adding*/
	private int counter;
	
	/**
	 * Input counter
	 * @param counter Times to adding char.
	 */
	public AppendToString(int counter)
	{
		this.counter = counter;
	}

	@Override
	/**
	 * Run task.
	 */
	public void run() 
	{
		
		String sum = ""; 
		for(int i = 0 ; i<counter ; i++)
		{
			sum += CHAR;
		}
		
	}
	
	/**
	 * return String information of this task
	 */
	public String toString()
	{
		return String.format("%s with count = %d",task,counter);
	}
	
}
