import java.io.*;
import static java.util.concurrent.TimeUnit.NANOSECONDS;;

/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class StopWatch {

	/**
	 * create variable for elapsed
	 */
	long initTime = 0 ,stopTime = 0;
	/**
	 * A boolean for check if is running
	 */
	private boolean isRunning;
	
	/**
	 * Constructor for initial value of is running to false
	 */
	public StopWatch()
	{
		this.isRunning = false;
	}

	/**
	 * Start stop watch by initial nano time to initTime
	 */
	public void start()
	{
		if(isRunning())
		{
			
		}
		else
		{
			this.initTime = System.nanoTime();
			this.isRunning = true;
		}
	}

	/**
	 * Stop stop watch by initial nano time to stopTime
	 */
	public void stop()
	{
		this.stopTime = System.nanoTime();
		this.isRunning = false;
	}

	/**
	 * Check if is running by return value of isRunning
	 * @return
	 */
	public boolean isRunning()
	{
		return this.isRunning;
	}

	/**
	 * get Elapsed time by 2 condition if running or not running.
	 * @return
	 */
	public double getElapsed()
	{
		if(isRunning())
		{
			return (double)(System.nanoTime() - this.initTime)/1000000000;
		}
		else
		{
			return (double)(this.stopTime-this.initTime)/1000000000;
		}
	}

}
