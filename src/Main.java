/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class Main {

	/**
	 * 
	 * Main method for run and timed 5tasks.
	 * 
	 */
	public static void main(String[] args)
	{
		/**Initial all 5 task*/
		AppendToString task1 = new AppendToString(100000);
		AppendToStringBuilder task2 = new AppendToStringBuilder(100000);
		SumDoublePrimitive task3 = new SumDoublePrimitive(100000000,500000);
		SumDouble task4 = new SumDouble(100000000,500000);
		SumBigDecimal task5 = new SumBigDecimal(100000000,500000);
		
		/**Initial our taskTimer*/
		TaskTimer timer = new TaskTimer();
		
		/**Timing our tasks*/
		timer.timedAndPrint(task1);
		timer.timedAndPrint(task2);
		timer.timedAndPrint(task3);
		timer.timedAndPrint(task4);
		timer.timedAndPrint(task5);
		
	}
	
}
