/**
 * Append to String speed test.
 * @author Woramate Jumroonsilp
 *
 */
public class AppendToStringBuilder implements Runnable{

	/**A char for add to String*/
	private static final char CHAR = 'a';
	/**Name of this task*/
	private String task = "Append to StringBuilder";
	/**Counter for value of adding*/
	private int counter;
	
	/**
	 * input counter.
	 * @param counter
	 */
	public AppendToStringBuilder(int counter)
	{
		this.counter = counter;
	}

	@Override
	/**
	 * Run task.
	 */
	public void run() 
	{
		
		StringBuilder SB = new StringBuilder();
		for(int i = 0 ; i<counter ; i++)
		{
			SB = SB.append(CHAR);
		}
		
	}
	
	/**
	 * return String information of this task
	 */
	public String toString()
	{
		return String.format("%s with count = %d",task,counter);
	}
}
