import java.math.BigDecimal;


public class SumBigDecimal implements Runnable{

	/**Counter for value of adding*/
	private int counter;
	/**Crate BigDecimal*/
	private BigDecimal[] bigDeci;
	/**Name of this task*/
	private String task = "Sum Big Decimal";

	/**
	 * Input counter and size of big decimal
	 * @param counter
	 * @param arrSize
	 */
	public SumBigDecimal(int counter , int arrSize)
	{
		this.counter  = counter;
		this.bigDeci = new BigDecimal[arrSize];
		for(int i = 0; i<arrSize ; i++){
			bigDeci[i] = new BigDecimal(i+1);
		}
	}

	@Override
	/**
	 * Run task.
	 */
	public void run() 
	{
		BigDecimal sum = new BigDecimal(0);
		for(int i = 0 , count = 0 ; count<counter ; i++ , count++){
			if(i>=bigDeci.length) i = 0;
			{
				sum = sum.add(bigDeci[i]);
			}
		}

	}

	/**
	 * return String information of this task
	 */
	public String toString()
	{
		return String.format("%s with count = %d , array size = %d",task,counter,bigDeci.length);
	}

}
