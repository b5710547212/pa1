
public class SumDoublePrimitive implements Runnable{

	/**Counter for value of adding*/
	private int counter;
	/**Creat array of double primitive typer*/
	private double[] arrDouble;
	/**Name of this task*/
	private String task = "Sum DoublePrimitive";
	
	/**
	 * Input counter and size of array
	 * @param counter
	 * @param arrSize
	 */
	public SumDoublePrimitive(int counter , int arrSize)
	{
		this.counter = counter;
		arrDouble = new double[arrSize];
		for(int i = 0 ; i<arrSize ; i++)
		{
			arrDouble[i] = i+1 ;
		}
	}

	@Override
	/**
	 * Run task.
	 */
	public void run() 
	{
		double sum = 0;
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= arrDouble.length) i = 0;
			sum = sum + arrDouble[i];
		}
		
	}
	
	/**
	 * return String information of this task
	 */
	public String toString()
	{
		return String.format("%s with count = %d , array Size = %d",task,counter,arrDouble.length);
	}
	
}
