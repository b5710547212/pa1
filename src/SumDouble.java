
public class SumDouble implements Runnable{

	/**Name of this task*/
	private final String task = "Sum Double";
	/**Counter for value of adding*/
	private int counter;
	/**Create array of double object*/
	private Double[] arrDouble;
	
	/**
	 * Input counter and size of array
	 * @param counter
	 * @param arrSize
	 */
	public SumDouble(int counter , int arrSize)
	{
		this.counter = counter;
		this.arrDouble = new Double[arrSize];
		for(int i = 0 ; i<arrSize ; i++)
		{
			arrDouble[i] = new Double(i+1);
		}
	}
	
	@Override
	/**
	 * Run task.
	 */
	public void run() 
	{
		Double sum = new Double(0);
		for(int i = 0 ; i<arrDouble.length ; i++)
		{
			sum = sum+arrDouble[i];
		}
		
	}
	
	/**
	 * return String information of this task
	 */
	public String toString()
	{
		return String.format("%s with count = %d , array size = %d",task,counter,arrDouble.length);
	}

}
